//////////////
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///Print 'Welcome' and <lehigh username> to terminal window
    ///Also prints a short autobiographical statement
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--B--F--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("Hi my name is Matthew Fest, I am a sophomore offensive lineman on the Lehigh football team majoring in statistics.");
    
    
  }
  
}