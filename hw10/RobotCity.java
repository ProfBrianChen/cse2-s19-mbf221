////////Program that creates a city then gets invaded by robots
// CSE 2,Lab08
import java.util.Random;
public class RobotCity{
   public static void main(String[] args) {
     Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
     int blocks = myrandom.nextInt(5)+10;//generatoring number of blocks laterally in the city
     int blocks1 = myrandom.nextInt(5)+10;//generatoring number of blocks vertically in the city
     int[][] cityArray = buildCity(blocks,blocks1);//generatoring the array that stores the populations of each block
     int k = myrandom.nextInt(5)+10;//generatoring the number of robots that are going to invade
     display(cityArray);//prints the blocks and their population in neat format
     System.out.println("Invasion by the robots... "); //Alerting the user of the invasion
	   display(invade(cityArray,k,blocks,blocks1));//displaying the new city and the blocks following the invasion
	   for(int i=0;i<5;i++){//for loop in order to run the methods 5 times
       System.out.println("Updating robots location... ");//alerting user of the updating coordinates
       update(cityArray, blocks1);//running the update method
       display(cityArray);}//displaying the new city and the blocks
   }
  
  public static int[][] buildCity(int blocks, int blocks1){//method that generates the populations for each block
    Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
    int[][] cityArray = new int [blocks][blocks1];//allocating the new array for the city
    for(int i=0;i<cityArray.length;i++){//for loop to input the populations in to the array
      for (int j=0;j<cityArray[i].length;j++){//for loop to input the populations in to the array
        cityArray[i][j] = myrandom.nextInt(899)+100;}}////accepting and storing the random integers
    return cityArray;//returning the array with the populations stored in the array
  }
  
  public static void display(int[][] cityArray){//method that prints the city and each blocks population is neat format
    for(int i=0;i<cityArray.length;i++){
      for(int j=0; j<cityArray[i].length;j++){
        System.out.printf(cityArray[i][j] + "\t");}//printing each block's population
      System.out.println();}
  }
  
  public static int[][] invade(int[][] cityArray, int k, int blocks, int blocks1){//method that invades
    Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
    int longitude;//declaring variables for vertical and horizontal components of coordinates
    int latitude;
    int[][] coordinates = new int[blocks][blocks1];//allocating new array to store which areas got invaded
    for (int i=0;i<k;i++){//generating coordinates for each robot to invade
      longitude = myrandom.nextInt(blocks);
      latitude = myrandom.nextInt(blocks1);
      if (coordinates[longitude][latitude]<0){//checking if the coordinates generated have been chosen before
        i--;}//if they have then the loop will regenerate the coordinates
      else{//if they have not then the array will store the negative value of the population to signify that it has been invaded
        coordinates[longitude][latitude] = -cityArray[blocks][blocks1];}}
    for (int i=0;i<blocks;i++){//filling the rest of the empty array with values from the original city array
      for (int j=0;j<blocks1;j++){
        if (coordinates[i][j] != -cityArray[i][j]){
        coordinates[i][j] = cityArray[i][j];}}}
      display(coordinates);//printing out the array for the invasion coordinates to show which areas were invaded
     return coordinates;
  }
  
 public static int [][] update (int[][] cityArray, int blocks1){//method to update the invasion location by the robots
   for (int i=cityArray.length-1; i>=0; i--){//moving the location of the robots to the right one
     for (int j=cityArray[i].length-1; j>=0; j--){
       if (cityArray[i][j]<0){//reseting the old invasion points to positive to show that they are not being invaded anymore
         cityArray[i][j]=-cityArray[i][j];
         if (j+1<blocks1){//setting the new invasion points to negative to show that they haeve been selected
           cityArray[i][j+1] = -cityArray[i][j+1];}}}}
   display(cityArray);//printing out the new array with the new coordinates
   return cityArray;
    }
}