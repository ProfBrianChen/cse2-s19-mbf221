////////Program that checks a poker hand for a straight
// CSE 2,Lab08
import java.util.Random;//importing random generator to avoid compiler error
public class Straight{  //beginning of class
   public static void main(String[] args){   //beginning of main method
     int check = 0;//declaring variable for the number of straights out of the 1,000,000 simulations
     int i=0;//declaring variable for number of simulations
     while(i<1000000){//while loop in order to run 1,000,000 simulations
       int[] shuffled = shuffled();//generating the shuffled deck of cards
       int[] hand = hand(shuffled);//selecting the first five cards in the shuffled deck
       boolean chance = straighttt(hand);//running method to check if there is a straight
       if (chance==true){//adding up the amount of straights we get in the simulation
         check++;}
       i++;//adding to variable so that the loop stops after 1000000 simulations
     }
       System.out.println("You got " + check + " straights.");//reading the results to the user
       System.out.println(" " + check/10000 + "% of hands are straights");
     
  }//end of main method
 


   public static int[] shuffled(){//method for shuffling the deck of cards
    Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
    int[] noshuff = new int[52];//allocating the unshuffled deck
    int[] shuff = new int[52];//allocating the shuffled deck
    for (int i=0; i<52; i++){ //setting the values for the unshuffled deck
      noshuff[i] = i; }
    for (int i=0; i<52; i++){//shuffling the deck
      int k = myrandom.nextInt(52); 
      shuff[k] = noshuff[i];}
    return shuff; //returning the shuffled deck
  } //end of shuffled method
  
   public static int[] hand(int[] shuffled){ //method for picking the first five cards out of the shuffled deck
     int[] hand = new int[5];//allocating the new array for the user's hand with size 5
     for (int i=0;i<5;i++){ //for loop selecting the first five cards out of the shuffled deck
       hand[i]=shuffled[i];}
     return hand; //returning the hand to the user
   }//end of hand method
  
  public static int search(int[] hand, int k){ //method for searching for the lowest cards at specific points
    if (k==1){//this loop looks for the smallest number in the hand
      int min1 = 1000;//setting the minimum in order to set up the search
      for(int i=0;i<hand.length;i++){//searching for the smallest number
        if(hand[i]<min1)//comparing values then resetting minimum
          hand[i]=min1;}
      return min1;}//returning the obtained value
    else if (k==2){//this loop looks for the second smallest number in the hand
      int min1 = 1000;//setting the minimums in order to set up the search
      int min2 = 1000;//setting the minimums in order to set up the search
      for (int i=0;i<hand.length;i++){//for loop searching for the second smallest number
        if (hand[i]<min1){//setting the minimums in order to set up the search
          min2 = min1;
          min1 = hand[i];}}
      return min2;}//returning the obtained value
    else if (k==3){//this loop looks for the third smallest number in the hand
      int min1 = 1000;//setting the minimums in order to set up the search
      int min2 = 1000;//setting the minimums in order to set up the search
      int min3 = 1000;//setting the minimums in order to set up the search
      for(int i=0;i<hand.length;i++){//for loop searching for the third smallest number
        if(hand[i]<min1){//setting the minimums in order to set up the search
          min3 = min2;
          min2 = min1;
          min1 = hand[i];}}
      return min3;}//returning the obtained value   
    else if (k==4){//this loop looks for the fourth smallest number in the hand
      int min1 = 1000;//setting the minimums in order to set up the search
      int min2 = 1000;//setting the minimums in order to set up the search
      int min3 = 1000;//setting the minimums in order to set up the search
      int min4 = 1000;//setting the minimums in order to set up the search
      for(int i=0;i<hand.length;i++){//for loop searching for the fourth smallest number
        if(hand[i]<min1){//setting the minimums in order to set up the search
          min4 = min3;
          min3 = min2;
          min2 = min1;
          min1 = hand[i];}}
      return min4;}//returning the obtained value 
    else if (k==5){//this loop looks for the fifth smallest number in the hand
      int min1 = 1000;//setting the minimums in order to set up the search
      int min2 = 1000;//setting the minimums in order to set up the search
      int min3 = 1000;//setting the minimums in order to set up the search
      int min4 = 1000;//setting the minimums in order to set up the search
      int min5 = 1000;//setting the minimums in order to set up the search
      for(int i=0;i<hand.length;i++){//for loop searching for the fifth smallest number
        if(hand[i]<min1){//setting the minimums in order to set up the search
          min5 = min4;
          min4 = min3;
          min3 = min2;
          min2 = min1;
          min1 = hand[i];}}
      return min5;}//returning the obtained value 
    else{
      return -1;}//returning -1 in the case of an error
  }//end of search method
  
   public static boolean straighttt(int[] hand){//method that checks if there is a straight in the user's hand
     int stra = 0;//declaring variable to find how many cards in a hand are consecutive
     for(int i=0; i<hand.length-1; i++){//for loop to go through the user's hand
       if(search(hand,i) == (search(hand,i+1))-1){//checking if the number's are consecutive by calling the search method
         stra++;}}//adding up the number of cards in a hand that are consecutive
    if(stra==hand.length-1){//returning true if there is a straight
      return true;}
    else{//returning false if there is not a straight
       return false;}
    
  }//end of straighttt method
  
}//end of class