//////////////
//// CSE 02 Arithmetic
///
public class Arithmetic{
  //main method required for every java program
  public static void main(String args[]){
int numPants = 3;          //Number of pairs of pants
double pantsPrice = 34.98; //Cost per pair of pants
    
int numShirts = 2;         //Number of sweatshirts
double shirtPrice = 24.99; //Cost per shirt
    
int numBelts = 1;          //Number of belts
double beltCost = 33.99;   //cost per belt
    
double paSalesTax = 0.06;  //the tax rate
double totalCostOfPants = numPants*pantsPrice;   //total cost of pants
double totalCostOfShirts = numShirts*shirtPrice;  //total cost of sweatshirts
double totalCostOfBelts = numBelts*beltCost;   //total cost of belts
double salesTaxPants = totalCostOfPants*paSalesTax; //total sales tax on pants
double salesTaxShirts = totalCostOfShirts*paSalesTax; //total sales tax on sweatshirts
double salesTaxBelts = totalCostOfBelts*paSalesTax; //total sales tax on belts
double totalCostPretax = totalCostOfBelts+totalCostOfShirts+totalCostOfPants; //total cost of purchase before taxes
double totalSalesTax = salesTaxBelts+salesTaxShirts+salesTaxPants; //total sales tax from all the items
double totalCost = totalCostPretax+totalSalesTax; //total cost of purchase including sales tax

double pretaxcost = totalCostPretax*100.00; //Calculations to truncate to only 2 decimal places for the total Cost pretax
int costpretax = (int) pretaxcost; //Have to convert double to an int to truncate
double totalCostPretaxtrunc = costpretax/100.00; //Multiply by 100 to get total pretax cost with only 2 decimal places
double salestax = totalSalesTax*100.00; //Calculations to truncate to only 2 decimal places for the total sales tax
int salestaxcost = (int) salestax; //Have to convert double to an int to truncate
double totalSalesTaxtrunc = salestaxcost/100.00; //Multiply by 100 to get total sales tax with only 2 decimal places
double costtotal = totalCost*100.00; //Calculations to truncate to only 2 decimal places for the total Cost
int totalcosttt = (int) costtotal; //Have to convert double to an int to truncate
double totalCosttrunc = totalcosttt/100.00; //Multiply by 100 to get total cost with only 2 decimal places
    
System.out.println("Pretax Cost: $"+totalCostPretaxtrunc); //display the total cost of the purchase pretax
System.out.println("Total Sales Tax: $"+totalSalesTaxtrunc); //display the total sales tax from all the items
System.out.println("Total Cost: $"+totalCosttrunc); //display the total cost of purchase including sales tax
    
  }
  
}