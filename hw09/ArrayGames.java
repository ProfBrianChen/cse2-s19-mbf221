////////Program that prompts user to decide what they want to do with arrays given two options
// CSE 2,Hw09
import java.util.Scanner; // Importing Scanner to avoid compiler errors
import java.util.Random; // Importing Random generator to avoid compiler errors
public class ArrayGames{
  // main method required for every Java program
    public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
      Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
      int size = myrandom.nextInt(10)+10; //Generating random size between 10 and 20 for arrays
      System.out.println("Enter 1 if you want to insert, or 2 if you want to shorten: ");//Asking the user if they would like to shorten an array or insert two arrays
      int result = myScanner.nextInt();//Receiving input from the user
      switch(result){//switch statement to determine what user wants to do
       case 1://if user wants to insert, this statement will run
        int[] user1 = generate(size);//Generating one of two arrays that will be inserted
        int[] user2 = generate(size);//Generating the second of two arrays that will be inserted
        System.out.print("The first array is: {" + user1[0]);//printing the first value of the array
        for (int i = 0; i<user1.length;i++){//for loop to print the values of the array
          System.out.print(", " + user1[i]);}
        System.out.println("}");//close bracket that succedes the printed out array
        System.out.print("The second array is: {" + user2[0]);//printing the first value of the array
        for (int i = 0; i<user2.length;i++){//for loop to print the values of the array
          System.out.print(", " + user2[i]);}
        System.out.println("}");//close bracket that succedes the printed out array
        int[] methodranarray = insert(user1, user2, size);//running the insert method and storing the returning value in an array
        System.out.print("The final inserted array is: ");
        print(methodranarray);//running the print method that prints the returned array
        break;//break statement to conclude this case
       case 2: //if user wants to shorten, this statement will run
        int[] genarray = generate(size);//Generating the array that will be shortened
        System.out.print("The original array is: {" + genarray[0]);
        for (int i=0; i<genarray.length;i++){//for loop to print the values of the array
          System.out.print(", " + genarray[i]);}
        System.out.println("}");//close bracket that succedes the printed out array
        shorten(genarray, size);//running the shorten method
        print(genarray);//printing the returned array from the shorten method
        break;//break statement to conclude this case
       default: //else statement in the case the user does not input one of the two valid values
        System.out.println("Sorry this is not a valid input, you will have to try to run the code again.");//Letting user know that their value wasnt valid and they will have to run the code again
        break;}//break statemennt that ends the code
      
      
  }//end of main method
  
    public static int[] generate(int size) {//generate method that generates random arrays based on the random size between 10 and 20
      Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
      int [] randomarray = new int[size]; //Declaring the array
      for (int i = 0; i<size;i++){//Generating the random values to fill the array
        randomarray[i] = myrandom.nextInt(size);}//Storing the values in the array
      return randomarray;//returning the randomly generated array
  
  } //end of generate method
    
    public static void print(int[] genarray) { //print method that will print out the array that it is told to
      System.out.print("{" + genarray[0]); //printing the first value of the array
      for(int i = 0;i<genarray.length;i++){//for loop to print the values of the array
        System.out.print(", " + genarray[i]);}
      System.out.println("}");//close bracket that succedes the printed out array
    
    } //end of print method
  
    public static int[] insert(int[] user1, int[] user2, int size){ //insert method that will combine two arrays at a random point
      Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
      int[] insertedarray = new int[user1.length + user2.length];//Declaring the new array that will store the arrays following the insertion
      int insertionpoint = myrandom.nextInt(size);//generating the insertion point of the array
      for (int i = 0; i<insertionpoint;i++){//printing the values of the first array up until the insertion point
            insertedarray[i] = user1[i];}
      for(int i = 0; i<user2.length;i++){//printing the values of the second array
            insertedarray[insertionpoint + i] = user2[i]; }
      for (int i = 0; i<insertedarray.length - user2.length - insertionpoint;i++){ //printing the remaining values of the first array to complete the new array
            insertedarray[i + user2.length + insertionpoint] = user1[i + insertionpoint];}
      return insertedarray; //returning the new array following the insertion
    
    } //end of insert method
    
    public static int[] shorten(int[] genarray, int size) { //shorten method that will remove a value from the array if prompted to by user input
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
      int[] shortenedarray = new int[genarray.length - 1]; //declaring the new array
      System.out.println("Enter an int to use as the index for the array: ");//asking the user to input the index
      int index = myScanner.nextInt();//accepting user input as the index
      if (index<genarray.length-1){//if statement that runs if the index entered by the user is less than the length of original array minus one because that would be the new arrays length
        for(int i = 0; i<shortenedarray.length;i++){//printing out the new array with one less value than the original array                        
          shortenedarray[i] = genarray[i+1];}}//storing all values from original array in the new array except the last one
      else{//else statement that returns the original array if the index is not high enough
        return genarray;}//returns original array
      return shortenedarray;//returns the new array 
      
      } //end of shorten method
  
  
  	} //end of class