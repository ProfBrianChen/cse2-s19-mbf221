////////Program to help a user play the lottery
// CSE 2,Hw08
import java.util.Scanner; // Importing Scanner to avoid compiler errors
import java.util.Random; // Importing Random generator to avoid compiler errors
public class PlayLottery{
  // main method required for every Java program
    public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
      int [] UserGuess = new int[5];//Creates the array based on user input
      System.out.println("Enter 5 integers from 0 to 59 please: ");//Asking user for input of his/her 5 numbers
      for(int i = 1; i<=5; i++){//for loop so that the array stores the next 5 integers input by the user
        UserGuess[i] = myScanner.nextInt(); }// stores value of user in the array
      int [] WinningNumbers = numbersPicked(); //stores values generated from the method that generates random numbers
      boolean WinLose = userWins(UserGuess, WinningNumbers); //declares variable to determine if user won or lost and calls method to check if numbers match
      if(WinLose==true){
        System.out.println("You won the lottery!");} //if the numbers match, program prints this to the user
      else{
        System.out.println("You did not win the lottery, better luck next time!");}//if the numbers do not match, program prints that to the user
  
      }//end of main method   
  
    public static boolean userWins(int[] usernumbers, int[] winningguess){ //returns true when user and winning are the same.
      int matching = 0; //declaring variable to see if numbers match
      for(int i = 1; i<=5; i++){//for loop to check if the users numbers match the random numbers generated
      if(usernumbers[i] == winningguess[i]){//checking if the 5 numbers match
        matching++ ; }} //adding to the matching variable to see how many match in the correct order
      if (matching == 5){//if statement to determine if all 5 match
        return true; } //returns true if all 5 match
      else{
        return false; }//returns false if all 5 dont match
    
  }//end of userWins method

    
    public static int[] numbersPicked(){ //method that generates the 5 winning numbers
      Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
      int [] winningguess = new int[5]; //creating an array to store the randomly generated winning numbers
      System.out.println("Winning Numbers: "); //Prints the winning numbers for the user
      for (int i = 1; i<=5; i++){
         winningguess[i] = myrandom.nextInt(60);//Generates the random numbers and adds to array
         System.out.println(winningguess[i] + " ");}//Prints the winning numbers for the user
      return winningguess; //returns the array to the main method where it was called
      

        
        
  }//end of numbersPicked method
  
  	} //end of class