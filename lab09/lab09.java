////////Program
// CSE 2,Lab08
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class lab09{
    public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
      Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
      System.out.println("Would you like a linear or binary search? Enter 1 for linear or 2 for binary.");
      int result = myScanner.nextInt();//Receiving input from the user
      System.out.println("How big do you want your array? Enter number below 100 please: ");
      int size = myScanner.nextInt();
      switch(result){//switch statement to determine what user wants to do
       case 1://if user wants to insert, this statement will run
        int[] array1 = generate(size);
        System.out.println("What number are you searching for? ");
        int pointnum = myScanner.nextInt(); 
        int location = searchlinear(array1, pointnum);
        if (location>=0){
          System.out.println("The number was found at index " + location);}
        else{
          System.out.println("The number cannot be found (i=-1).");}
       case 2:
         int[] array2=ascendArray(size);
         System.out.println("What number are you searching for? ");
         int pointnum1 = myScanner.nextInt();
         int binlocation = binarySearch(array2, pointnum1);
         if (binlocation>=0){
            System.out.println("The number was found at index " + binlocation);}
         else{
            System.out.println("The number cannot be found (i=-1).");}}
      
    }
      
      public static int[] generate(int size) {//generate method that generates random arrays based on the random size between 10 and 20
        Random myrandom = new Random(); //Telling random generator that I am creating an instance that will take input from STDIN
        int [] randomarray = new int[size]; //Declaring the array
        for (int i = 0; i<size;i++){//Generating the random values to fill the array
          randomarray[i] = myrandom.nextInt(size);}//Storing the values in the array
        return randomarray;//returning the randomly generated array
  
  } //end of generate method
    
      public static int[] ascendArray(int size){
        Random randomGenerator = new Random(); 
        Scanner myScanner = new Scanner(System.in); 
        int[] ascendingArray=new int[size];
        for(int i=0; i<size; i++){ 
          ascendingArray[i]=randomGenerator.nextInt(size);} 
        Arrays.sort(ascendingArray);
        for (int i=0; i<size; i++){
          System.out.print(ascendingArray[i]+" ");}
        return ascendingArray;
        
   }
       
      public static int searchlinear(int[] array1, int pointnum){
        for(int i=0; i<array1.length; i++){    
          if(array1[i] == pointnum){    
            return i;}}
        return -1;
   }
  
      public static int binarySearch(int[] array2, int pointnum1){
        int binlocation = 0;
        int low = 0;
        int high = array2.length-1;
        while (low<=high){
          int ave = (high+low)/2;
          if (array2[ave]<pointnum1){
            low = ave + 1;}
          else if (array2[ave]>pointnum1){
            high = ave-1;}
          else if (array2[ave]==pointnum1){
            binlocation = ave;
            return binlocation;}}
        return -1;
   }
 
}