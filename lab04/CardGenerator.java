////////Program that randomly selects card from 52 card deck and prints the suit and card.
// CSE 2, CardGenerator
public class CardGenerator{
    	  // main method required for every Java program
    	public static void main(String[] args) {
        double random = (Math.random()*51)+1; //Randomly selecting a number between 1 and 52, including 1 and 52
        double randomcard = (int)random; //Casting the random selection as an integer
        double remainder = randomcard%10;//Using Modular function in order to get remainder
        
        if (2<=randomcard & randomcard<=10){ //Using if statement to determine if random card is a numbered diamonds
        System.out.println("You picked the "+randomcard+" of diamonds.");}//If it is a numbered diamonds then the program will print the card for user
        
        if (15<=randomcard & randomcard<=23){//Using if statement to determine if random card is a numbered clubs
        double randomclubs = randomcard-13; //Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("You picked the "+randomclubs+" of clubs.");}//If it is a numbered clubs then the program will print the card for user
        
        if (28<=randomcard & randomcard<=36){//Using if statement to determine if random card is a numbered hearts
        double randomhearts = randomcard-26;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("You picked the "+randomhearts+" of hearts.");}//If it is a numbered hearts then the program will print the card for user
        
        if (41<=randomcard & randomcard<=49){//Using if statement to determine if random card is a numbered spades
        double randomspades = randomcard-39;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("You picked the "+randomspades+" of spades.");}//If it is a numbered spades then the program will print the card for user
        
        if (randomcard==1){//Using if statement to determine if random card is the ace of diamonds
          System.out.println("You picked the ace of diamonds.");}//If if statement is true then program prints the card for user
        if (randomcard==14){//Using if statement to determine if random card is the ace of clubs
          System.out.println("You picked the ace of clubs.");}//If if statement is true then program prints the card for user
        if (randomcard==27){//Using if statement to determine if random card is the ace of hearts
          System.out.println("You picked the ace of hearts.");}//If if statement is true then program prints the card for user
        if (randomcard==40){//Using if statement to determine if random card is the ace of spades
          System.out.println("You picked the ace of spades.");}//If if statement is true then program prints the card for user
        
        if (remainder==1 && randomcard<14 && randomcard>3){ //Using if statement to determine if random card is a face card, specifically a jack of diamonds for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("You picked the jack of diamonds");}//Printing the card if the statement is true meaning that it is the jack of diamonds
          else if (remainder==2 && randomcard<14 && randomcard>3){//Using an else if statement to determine if random card is the queen of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("You picked the queen of diamonds");}//Printing the card if the statement is true meaning that it is the queen of diamonds
          else if (remainder==3 && randomcard<14 && randomcard>3){//Using an else if statement to determine if random card is the king of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("You picked the king of diamonds");}//Printing the card if the statement is true meaning that it is the king of diamonds
        if (remainder==4 && randomcard<27 && randomcard>16){//Using if statement to determine if random card is a face card, specifically a jack of clubs for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("You picked the jack of clubs");}//Printing the card if the statement is true meaning that it is the jack of clubs
          else if (remainder==5 && randomcard<27 && randomcard>16){//Using an else if statement to determine if random card is the queen of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("You picked the queen of clubs");}//Printing the card if the statement is true meaning that it is the queen of clubs
          else if (remainder==6 && randomcard<27 && randomcard>16){//Using an else if statement to determine if random card is the king of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("You picked the king of clubs");}//Printing the card if the statement is true meaning that it is the king of clubs
        if (remainder==7 && randomcard<40 && randomcard>29){//Using if statement to determine if random card is a face card, specifically a jack of hearts for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("You picked the jack of hearts");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder==8 && randomcard<40 && randomcard>29){//Using an else if statement to determine if random card is the queen of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("You picked the queen of hearts");}//Printing the card if the statement is true meaning that it is the queen of hearts
          else if (remainder==9 && randomcard<40 && randomcard>29){//Using an else if statement to determine if random card is the king of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("You picked the king of hearts");}//Printing the card if the statement is true meaning that it is the king of hearts
        if (remainder==0 && randomcard>44){//Using if statement to determine if random card is a face card, specifically a jack of spades for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("You picked the jack of spades");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder==1 && randomcard>44){//Using an else if statement to determine if random card is the queen of spades by using the remainder and specific range of numbers for spades
          System.out.println("You picked the queen of spades");}//Printing the card if the statement is true meaning that it is the queen of spades
          else if (remainder==2 && randomcard>44){//Using an else if statement to determine if random card is the king of spades by using the remainder and specific range of numbers for spades
          System.out.println("You picked the king of spades");}//Printing the card if the statement is true meaning that it is the king of spades
                    
}  //end of main method   
  	} //end of class