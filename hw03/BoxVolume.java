////////Program that takes input from the user trying to solve for the volume of a box given length, width, height
// CSE 2, BoxVolume
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class BoxVolume{
    			// main method required for every Java program
   			public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); //Telling scanner that I am creating an instance that will take input from STDIN
  System.out.print("The width side of the box is: "); //Prompting the user to input the width side of the box
  double width = myScanner.nextDouble(); //Accepting user input for the width side of the box
  System.out.print("The length of the box is: "); //Prompting the user to input the length of the box
  double length = myScanner.nextDouble(); //Accepting user input for the length side of the box
  System.out.print("The height of the box is: "); //Prompting the user to input the height side of the box
  double height = myScanner.nextDouble(); //Accepting user input for the height side of the box
  double Volume = width*length*height; //Calculation for volume, multiplying length, width, and height
  System.out.println("The volume inside the box is: " + Volume); //Printing the resulting Volume
          
                    
}  //end of main method   
  	} //end of class