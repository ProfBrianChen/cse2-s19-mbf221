////////Program that takes input from the user trying to convert meters to inches
// CSE 2, Convert
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); //Telling scanner that I am creating an instance that will take input from STDIN
  System.out.print("Enter the distance in meters: "); //Prompting the user to input the distance in meters
  double distanceMeters = myScanner.nextDouble(); //Accepting user input for the distance in meters
  double distanceInches = distanceMeters*39.3700789; //Calculation for converting meters to inches
  System.out.println(distanceMeters + " meters is "+ distanceInches + " inches."); //Printing the resulting conversion in inches
          
                    
}  //end of main method   
  	} //end of class