////////Program that takes input from the user trying to figure out how to evenly split the check and gives the output to the user
// CSE 2, Check
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); //Telling scanner that I am creating an instance that will take input from STDIN
  System.out.print("Enter the original cost of the check in the form xx.xx: "); //Prompting the user to input original cost of the check
  double checkCost = myScanner.nextDouble(); //Accepting user input for the original cost of the check
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):"); //Prompting the user to input their desired tip percentage
  double tipPercent = myScanner.nextDouble(); //Accepting the user input for their desired tip percentage
  tipPercent /= 100; //We want to convert the percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner:"); //Asking the user to input the number of people that they're splitting the check between
  int numPeople = myScanner.nextInt(); //Accepting the user input for the number of people splitting the check at dinner
  double totalCost; //Declaring the variable for the total cost of the dinner
  double costPerPerson; //Declaring the variable for the cost for each person eating dinner
  int dollars,   //whole dollar amount of cost 
  dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
  totalCost = checkCost * (1 + tipPercent); //Formula for calculating the total cost of the dinner
  costPerPerson = totalCost / numPeople; //Formula for calculating the cost per person
    //get the whole amount, dropping decimal fraction
  dollars=(int)costPerPerson; //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
  dimes=(int)(costPerPerson * 10) % 10; //Calculating the change, dimes in this case, owed by each person
  pennies=(int)(costPerPerson * 100) % 10; //Calculating the change, pennies in this case, owed by each person
  System.out.println("Each person in the group owes $"+ dollars + "." + dimes + pennies); //Printing the result showing how much each person owes
          
          
}  //end of main method   
  	} //end of class

