////////Program that asks for user input then prints out a triangle based on the users input
// CSE 2,PatternD
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class PatternD{
    	  // main method required for every Java program
    	public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
      System.out.println("Please enter an int 1-10: "); //Asking the user to input desired triangle parameters
      int triSize; //Initializing variable for length
      while (!myScanner.hasNextInt()) { //While loop to confirm that user input is valid
          System.out.println("This is not a valid answer, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
          myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
      }
      triSize = myScanner.nextInt(); //Accepting valid input once the while loop is complete
        
        for(int i=1; i<=triSize; triSize--) {
            for(int j=triSize; j>=i; --j) {
                System.out.print(j + " ");
            }
            System.out.println();
          
        }
      
       
                            
}  //end of main method   
  	} //end of class