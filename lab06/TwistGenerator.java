////////Program that asks for user input then prints out a simple "twist" based on user input
// CSE 2,TwistGenerator
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class TwistGenerator{
    	  // main method required for every Java program
    	public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
      System.out.println("Please enter a positive length: "); //Asking the user to input length
      int length; //Initializing variable for length
      while (!myScanner.hasNextInt()) { //While loop to confirm that user input is valid
          System.out.println("This is not an int, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
          myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
      }
      length = myScanner.nextInt(); //Accepting valid input once the while loop is complete
      int lengthx = length; //Declaring variable for middle row of X's
      int lengthbottom = length; //Declaring variable for bottom row of twist
        
      while (length>0){ //While loop to print out the top row of twist for length entered
        System.out.print("\\");//Printing out the first piece of the top row of the twist
        length--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
        System.out.print(" ");//Printing out the second piece of the top row of the twist
        length--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
        System.out.print("/");//Printing out the third piece of the top row of the twist
        length--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
      }
        System.out.println(); //Going to the next line so there are three rows of twist
        
        while (lengthx>0){//While loop to print out the middle row of twist for length entered
        System.out.print(" ");//Printing out the first piece of the middle row of the twist
        lengthx--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
        System.out.print("X");//Printing out the second piece of the middle row of the twist
        lengthx--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
        System.out.print(" ");//Printing out the third piece of the middle row of the twist
        lengthx--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
      }
        System.out.println(); //Going to the next line so there are three rows of twist
        
        while (lengthbottom>0){//While loop to print out the bottom row of twist for length entered
        System.out.print("/");//Printing out the first piece of the last row of the twist
        lengthbottom--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
        System.out.print(" ");//Printing out the second piece of the last row of the twist
        lengthbottom--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
        System.out.print("\\");//Printing out the third piece of the last row of the twist
        lengthbottom--;//Subtracting one off of the length entered so the loop only goes for the exact length entered
      }
        
       
                           
}  //end of main method   
  	} //end of class