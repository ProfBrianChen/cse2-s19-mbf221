////////Program that asks for user input of shapes and dimensions then computes area
// CSE 2,Area
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class Area{
    	  // main method required for every Java program
    	public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
        
      System.out.println("Would you like a rectangle, triangle, or circle?: ");//Asking user which shape they prefer to find area for
      String shape = myScanner.next(); //Accepting valid input once the while loop is complete
      double areaofshape = 0; //Declaring and initializing variable for area
      
      if (shape.equals("rectangle")){//Statement that activates if user inputs "rectangle"
        System.out.println("Please enter length: ");//Asking the user to input the rectangle length
        double length = check();//Storing input to check if it is a double
        System.out.println("Please enter width: ");//Asking the user to input the rectangle width
        double width = check(); //Storing input to check if it is a double
        areaofshape = rectangle(length, width);}//storing inputs in variable in order to compute area
      else if (shape.equals("triangle")){//Statement that activates if user inputs "triangle"
        System.out.println("Please enter length: ");//Asking the user to input the triangle length
        double trilength = check();//Storing input to check if it is a double
        System.out.println("Please enter height: ");//Asking the user to input the triangle height
        double triheight = check();//Storing input to check if it is a double
        areaofshape = triangle(trilength, triheight);}//storing inputs in variable in order to compute area
      else if (shape.equals("circle")){//Statement that activates if user inputs "circle"
        System.out.println("Please enter radius: ");//Asking the user to input the circle radius
        double radius = check();//Storing input to check if it is a double
        areaofshape = circle(radius);}//storing inputs in variable in order to compute area
      else {//else statement to tell user that their input for shape was not valid
        System.out.println("This is not a valid shape. please enter rectangle, triangle, or circle: ");}
        
        System.out.println("The area is " + areaofshape);//Printing out the area
      }//end of main method
  
      public static double rectangle(double length, double width) {//method for rectangle area
        return (length*width);}//computing and returning the area
        
      public static double triangle(double trilength, double triheight) {//method for triangle area
        return(trilength*triheight*.5);}//computing and returning the area
        
      public static double circle(double radius) {//method for circle area
        return(radius*radius*(3.14));}//computing and returning the area
        
      public static double check() {//method for checking if input is a double
        double input;//declaring variable for to check the user input
        Scanner checkScanner = new Scanner( System.in );//Telling scanner that I am creating an instance that will take input from STDIN
        while (checkScanner.hasNextDouble() == false) {//while statement to check the input by the user and continue to ask for input if not a double
           System.out.print("The value you entered is not a double");}//Telling the user that their input was not valid
        input = checkScanner.nextDouble();//Checking the input by the user
        return input;}//returning the proper output determining if user input was valid                     
 } //end of class