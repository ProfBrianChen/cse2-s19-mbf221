//////////////
//// CSE 02 Cyclometer
///
public class Cyclometer{
    	// main method required for every Java program
   	public static void main(String[] args) {
    int secsTrip1=480;  //This variable shows the amount of seconds that the first trip takes
    int secsTrip2=3220;  //This variable shows the amount of seconds that the second trip takes
		int countsTrip1=1561;  //This variable shows the amount of rotations the front wheel makes during the time of the first trip
		int countsTrip2=9037; //This variable shows the amount of rotations the front wheel makes during the time of the second trip
    double wheelDiameter=27.0,  //This variable holds the value of the diameter of the wheel which will be using in calculating the amount of rotations
  	PI=3.14159, //PI holds the number pi, which will be helpful in making calculations for rotations
  	feetPerMile=5280,  //This variable the value for the amount of feet in a mile, helpful for calculating the distance travelled in terms of miles
  	inchesPerFoot=12,   //This variable shows the value for inches in one foot, which can help to calculate distance in  units other than miles and feet
  	secondsPerMinute=60;  //This is variable shows the value for seconds in a minute which can be helpful in determining how many minutes one trip was
	  double distanceTrip1, distanceTrip2,totalDistance;  //declaring variables for distance travelled in each trip and total
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");//Prints the computation of time and rotations for trip 1
	  System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");//Prints the computation of time and rotations for trip 2
    distanceTrip1=countsTrip1*wheelDiameter*PI;//Computation for distance traveled for trip 1
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles for trip 1
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//Give distance in miles for trip 2, included is computation for distance travelled
	  totalDistance=distanceTrip1+distanceTrip2;//Gives the total distance of both trips combined
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");//Prints the total distance for trip 1 in miles
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");//Prints the total distance for trip 2 in miles
	  System.out.println("The total distance was "+totalDistance+" miles");//Prints the total distance for both trips combined in miles

      
	}  //end of main method   
} //end of class