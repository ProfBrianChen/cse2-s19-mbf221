////////Program
// CSE 2,Lab07
import java.util.Random; // Importing Random to avoid compiler errors
import java.util.Scanner; 
public class Lab07{
    	  // main method required for every Java program  
    	public static String adjective() {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String adj = "";
        switch (randomInt){
          case 0:
          adj = "pretty";
          break;
          case 1:
          adj = "mean";
          break;
          case 2:
          adj = "hard-working";
          break;
          case 3:
          adj = "scary";
          break;
          case 4:
          adj = "old";
          break;
          case 5:
          adj = "young";
          break;
          case 6:
          adj = "beautiful";
          break;
          case 7:
          adj = "hungry";
          break;
          case 8:
          adj = "weird";
          break;
          case 9:
          adj = "nice";
          break;
          case 10:
          adj = "caring";
          break;
        }
         return adj;
}
    
    public static String nounsubject(){
      Random randomGenerator1 = new Random();
        int randomInt1 = randomGenerator1.nextInt(10);
        String subject = "";
        switch (randomInt1){
          case 0:
          subject = "woman";
          break;
          case 1:
          subject = "man";
          break;
          case 2:
          subject = "guy";
          break;
          case 3:
          subject = "girl";
          break;
          case 4:
          subject = "boy";
          break;
          case 5:
          subject = "student";
          break;
          case 6:
          subject = "worker";
          break;
          case 7:
          subject = "construction worker";
          break;
          case 8:
          subject = "wrestler";
          break;
          case 9:
          subject = "teacher";
          break;
          case 10:
          subject = "janitor";
          break;
        }
          return subject;
}  
      public static String pasttenseverb(){
      Random randomGenerator2 = new Random();
        int randomInt2 = randomGenerator2.nextInt(10);
        String verb = "";
        switch (randomInt2){
          case 0:
          verb = "ate";
          break;
          case 1:
          verb = "ran";
          break;
          case 2:
          verb = "smoked";
          break;
          case 3:
          verb = "cleaned";
          break;
          case 4:
          verb = "turned";
          break;
          case 5:
          verb = "hit";
          break;
          case 6:
          verb = "worked";
          break;
          case 7:
          verb = "played";
          break;
          case 8:
          verb = "wore";
          break;
          case 9:
          verb = "learned";
          break;
          case 10:
          verb = "built";
          break;
        }
          return verb;
}  
    public static String nounobject(){
      Random randomGenerator3 = new Random();
        int randomInt3 = randomGenerator3.nextInt(10);
        String object = "";
        switch (randomInt3){
          case 0:
          object = "road";
          break;
          case 1:
          object = "car";
          break;
          case 2:
          object = "food";
          break;
          case 3:
          object = "dog";
          break;
          case 4:
          object = "tree";
          break;
          case 5:
          object = "computer";
          break;
          case 6:
          object = "shirt";
          break;
          case 7:
          object = "game";
          break;
          case 8:
          object = "table";
          break;
          case 9:
          object = "jacket";
          break;
          case 10:
          object = "backpack";
          break;
        }
          return object;
}  
   
    public static void makesentence(){
      Scanner myScanner = new Scanner(System.in);
      while (true) {
      String adj = adjective();
      String object = nounobject();
      String verb = pasttenseverb();
      String subject = nounsubject();

      System.out.println("The "+adjective()+" "+nounsubject()+" "+pasttenseverb()+" the "+nounobject()+".");
      System.out.println("And then the "+adjective()+" "+nounsubject()+" quit and "+pasttenseverb()+" "+nounobject()+".");     
      System.out.println("Enter 0 to continue. Enter any other number to exit: ");
      int cont = myScanner.nextInt();
      if (cont != 0) {
        break;}
        }
    }
  
      public static void main(String[] args){
        makesentence();
        
      }
  
  
  
  	} //end of class