////////Program that asks for user input on course they are taking
// CSE 2,Hw05
import java.util.Scanner; // Importing Scanner to avoid compiler errors
public class Hw05{
  // main method required for every Java program
    	public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in); //Telling scanner that I am creating an instance that will take input from STDIN
        String department; //Declaring variable for department
        int coursenumber;//Declaring variable for course number
        int timesperweek; //Declaring variable for times per week
        int starttime; //Declaring variable for start time
        String professor; //Declaring variable for professor
        int students; //Declaring variable for students
        
         System.out.println("What department is your class in? "); //Asking student to input his/her department
          while (myScanner.hasNextInt()||myScanner.hasNextDouble()){ //While loop to confirm that user input is valid
            System.out.println("This is not a string, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
            myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
          }
        department = myScanner.next(); //Accepting valid input once the while loop is complete
        
        System.out.println("Whats the course number for your class? ");//Asking student to input his/her course number
        while (!myScanner.hasNextInt()) { //While loop to confirm that user input is valid
          System.out.println("This is not an int, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
          myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
      }
        coursenumber = myScanner.nextInt(); //Accepting valid input once the while loop is complete
        
        System.out.println("How many times do you meet per week? ");//Asking student to input how many times his/her class meets
        while (!myScanner.hasNextInt()) { //While loop to confirm that user input is valid
          System.out.println("This is not an int, please try again: "); //If the response is invalid then we ask the user to input again until the response is valid
          myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
      }
        timesperweek = myScanner.nextInt(); //Accepting valid input once the while loop is complete
        
        System.out.println("Whats time does your class start? ");//Asking student to input his/her start time
        while (!myScanner.hasNextInt()) { //While loop to confirm that user input is valid
          System.out.println("This is not an int, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
          myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
      }
        starttime = myScanner.nextInt(); //Accepting valid input once the while loop is complete
        
        System.out.println("Who is your professor for this class? ");//Asking student to input his/her professor
          while (myScanner.hasNextInt()||myScanner.hasNextDouble()){ //While loop to confirm that user input is valid
            System.out.println("This is not a string, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
            myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
          }
        professor = myScanner.next(); //Accepting valid input once the while loop is complete
        
        System.out.println("How many students are in your class? ");//Asking student to input the amount of students in his/her class
        while (!myScanner.hasNextInt()) { //While loop to confirm that user input is valid
          System.out.println("This is not an int, please try again: ");//If the response is invalid then we ask the user to input again until the response is valid
          myScanner.next(); //clearing the scanner so that the user can continue to input until they enter a valid response
      }
        students = myScanner.nextInt(); //Accepting valid input once the while loop is complete
        
        
        
        }  //end of main method   
  	} //end of class