////////Program that randomly selects 5 cards from 52 card deck and tells whether you have a pair, two pair, or three of a kind. 
// CSE 2, PokerHandCheck
public class PokerHandCheck{
    	  // main method required for every Java program
    	public static void main(String[] args) {
        System.out.println("Your random cards were:");     	
        
        double random = (Math.random()*51)+1; //Randomly selecting a number between 1 and 52, including 1 and 52
        double randomcard = (int)random; //Casting the random selection as an integer
        double remainder = randomcard%10;//Using Modular function in order to get remainder
          
        if (2<=randomcard & randomcard<=10){ //Using if statement to determine if random card is a numbered diamonds
        System.out.println("the "+(int)randomcard+" of diamonds.");}//If it is a numbered diamonds then the program will print the card for user
        
        if (15<=randomcard & randomcard<=23){//Using if statement to determine if random card is a numbered clubs
        double randomclubs = randomcard-13; //Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomclubs+" of clubs.");}//If it is a numbered clubs then the program will print the card for user
        
        if (28<=randomcard & randomcard<=36){//Using if statement to determine if random card is a numbered hearts
        double randomhearts = randomcard-26;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomhearts+" of hearts.");}//If it is a numbered hearts then the program will print the card for user
        
        if (41<=randomcard & randomcard<=49){//Using if statement to determine if random card is a numbered spades
        double randomspades = randomcard-39;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomspades+" of spades.");}//If it is a numbered spades then the program will print the card for user
        
        switch ((int)randomcard){//Using switch statement to determine if random card is the ace of diamonds
          case 1://Within switch statement, determining whether random card is an ace of diamonds
          System.out.println("the ace of diamonds.");//If case statement is true then program prints the card for user
          break; //Putting break in statement allows the switch statement to move on to the next case
          case 14: //Within switch statement, determining whether random card is an ace of clubs
          System.out.println("the ace of clubs.");//If case statement is true then program prints the card for user
          break;
          case 27://Within switch statement, determining whether random card is an ace of hearts
          System.out.println("the ace of hearts.");//If case statement is true then program prints the card for user
          break;
          case 40: //Within switch statement, determining whether random card is an ace of spades
          System.out.println("the ace of spades.");//If case statement is true then program prints the card for user
          break;}
            
        if (remainder==1 && randomcard<14 && randomcard>3){ //Using if statement to determine if random card is a face card, specifically a jack of diamonds for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of diamonds");}//Printing the card if the statement is true meaning that it is the jack of diamonds
          else if (remainder==2 && randomcard<14 && randomcard>3){//Using an else if statement to determine if random card is the queen of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the queen of diamonds");}//Printing the card if the statement is true meaning that it is the queen of diamonds
          else if (remainder==3 && randomcard<14 && randomcard>3){//Using an else if statement to determine if random card is the king of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the king of diamonds");}//Printing the card if the statement is true meaning that it is the king of diamonds
        if (remainder==4 && randomcard<27 && randomcard>16){//Using if statement to determine if random card is a face card, specifically a jack of clubs for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of clubs");}//Printing the card if the statement is true meaning that it is the jack of clubs
          else if (remainder==5 && randomcard<27 && randomcard>16){//Using an else if statement to determine if random card is the queen of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the queen of clubs");}//Printing the card if the statement is true meaning that it is the queen of clubs
          else if (remainder==6 && randomcard<27 && randomcard>16){//Using an else if statement to determine if random card is the king of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the king of clubs");}//Printing the card if the statement is true meaning that it is the king of clubs
        if (remainder==7 && randomcard<40 && randomcard>29){//Using if statement to determine if random card is a face card, specifically a jack of hearts for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of hearts");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder==8 && randomcard<40 && randomcard>29){//Using an else if statement to determine if random card is the queen of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the queen of hearts");}//Printing the card if the statement is true meaning that it is the queen of hearts
          else if (remainder==9 && randomcard<40 && randomcard>29){//Using an else if statement to determine if random card is the king of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the king of hearts");}//Printing the card if the statement is true meaning that it is the king of hearts
        if (remainder==0 && randomcard>44){//Using if statement to determine if random card is a face card, specifically a jack of spades for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of spades");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder==1 && randomcard>44){//Using an else if statement to determine if random card is the queen of spades by using the remainder and specific range of numbers for spades
          System.out.println("the queen of spades");}//Printing the card if the statement is true meaning that it is the queen of spades
          else if (remainder==2 && randomcard>44){//Using an else if statement to determine if random card is the king of spades by using the remainder and specific range of numbers for spades
          System.out.println("the king of spades");}//Printing the card if the statement is true meaning that it is the king of spades
          
        //Second Card Below
        
        double random1 = (Math.random()*51)+1; //Randomly selecting a number between 1 and 52, including 1 and 52
        double randomcard1 = (int)random1; //Casting the random selection as an integer
        double remainder1 = randomcard1%10;//Using Modular function in order to get remainder
          
        if (2<=randomcard1 & randomcard1<=10){ //Using if statement to determine if random card is a numbered diamonds
        System.out.println("the "+(int)randomcard1+" of diamonds.");}//If it is a numbered diamonds then the program will print the card for user
        
        if (15<=randomcard1 & randomcard1<=23){//Using if statement to determine if random card is a numbered clubs
        double randomclubs1 = randomcard1-13; //Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomclubs1+" of clubs.");}//If it is a numbered clubs then the program will print the card for user
        
        if (28<=randomcard1 & randomcard1<=36){//Using if statement to determine if random card is a numbered hearts
        double randomhearts1 = randomcard1-26;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomhearts1+" of hearts.");}//If it is a numbered hearts then the program will print the card for user
        
        if (41<=randomcard1 & randomcard1<=49){//Using if statement to determine if random card is a numbered spades
        double randomspades1 = randomcard1-39;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomspades1+" of spades.");}//If it is a numbered spades then the program will print the card for user
        
        switch ((int)randomcard1){//Using switch statement to determine if random card is the ace of diamonds
          case 1://Within switch statement, determining whether random card is an ace of diamonds
          System.out.println("the ace of diamonds.");//If case statement is true then program prints the card for user
          break; //Putting break in statement allows the switch statement to move on to the next case
          case 14: //Within switch statement, determining whether random card is an ace of clubs
          System.out.println("the ace of clubs.");//If case statement is true then program prints the card for user
          break;
          case 27://Within switch statement, determining whether random card is an ace of hearts
          System.out.println("the ace of hearts.");//If case statement is true then program prints the card for user
          break;
          case 40: //Within switch statement, determining whether random card is an ace of spades
          System.out.println("the ace of spades.");//If case statement is true then program prints the card for user
          break;}
            
        if (remainder1==1 && randomcard1<14 && randomcard1>3){ //Using if statement to determine if random card is a face card, specifically a jack of diamonds for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of diamonds");}//Printing the card if the statement is true meaning that it is the jack of diamonds
          else if (remainder1==2 && randomcard1<14 && randomcard1>3){//Using an else if statement to determine if random card is the queen of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the queen of diamonds");}//Printing the card if the statement is true meaning that it is the queen of diamonds
          else if (remainder1==3 && randomcard1<14 && randomcard1>3){//Using an else if statement to determine if random card is the king of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the king of diamonds");}//Printing the card if the statement is true meaning that it is the king of diamonds
        if (remainder1==4 && randomcard1<27 && randomcard1>16){//Using if statement to determine if random card is a face card, specifically a jack of clubs for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of clubs");}//Printing the card if the statement is true meaning that it is the jack of clubs
          else if (remainder1==5 && randomcard1<27 && randomcard1>16){//Using an else if statement to determine if random card is the queen of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the queen of clubs");}//Printing the card if the statement is true meaning that it is the queen of clubs
          else if (remainder1==6 && randomcard1<27 && randomcard1>16){//Using an else if statement to determine if random card is the king of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the king of clubs");}//Printing the card if the statement is true meaning that it is the king of clubs
        if (remainder1==7 && randomcard1<40 && randomcard1>29){//Using if statement to determine if random card is a face card, specifically a jack of hearts for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of hearts");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder1==8 && randomcard1<40 && randomcard1>29){//Using an else if statement to determine if random card is the queen of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the queen of hearts");}//Printing the card if the statement is true meaning that it is the queen of hearts
          else if (remainder1==9 && randomcard1<40 && randomcard1>29){//Using an else if statement to determine if random card is the king of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the king of hearts");}//Printing the card if the statement is true meaning that it is the king of hearts
        if (remainder1==0 && randomcard1>44){//Using if statement to determine if random card is a face card, specifically a jack of spades for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of spades");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder1==1 && randomcard1>44){//Using an else if statement to determine if random card is the queen of spades by using the remainder and specific range of numbers for spades
          System.out.println("the queen of spades");}//Printing the card if the statement is true meaning that it is the queen of spades
          else if (remainder1==2 && randomcard1>44){//Using an else if statement to determine if random card is the king of spades by using the remainder and specific range of numbers for spades
          System.out.println("the king of spades");}//Printing the card if the statement is true meaning that it is the king of spades
        
        //Third Card Below
        
        double random2 = (Math.random()*51)+1; //Randomly selecting a number between 1 and 52, including 1 and 52
        double randomcard2 = (int)random2; //Casting the random selection as an integer
        double remainder2 = randomcard2%10;//Using Modular function in order to get remainder
          
        if (2<=randomcard2 & randomcard2<=10){ //Using if statement to determine if random card is a numbered diamonds
        System.out.println("the "+(int)randomcard2+" of diamonds.");}//If it is a numbered diamonds then the program will print the card for user
        
        if (15<=randomcard2 & randomcard2<=23){//Using if statement to determine if random card is a numbered clubs
        double randomclubs2 = randomcard2-13; //Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomclubs2+" of clubs.");}//If it is a numbered clubs then the program will print the card for user
        
        if (28<=randomcard2 & randomcard2<=36){//Using if statement to determine if random card is a numbered hearts
        double randomhearts2 = randomcard2-26;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomhearts2+" of hearts.");}//If it is a numbered hearts then the program will print the card for user
        
        if (41<=randomcard2 & randomcard2<=49){//Using if statement to determine if random card is a numbered spades
        double randomspades2 = randomcard2-39;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomspades2+" of spades.");}//If it is a numbered spades then the program will print the card for user
        
        switch ((int)randomcard2){//Using switch statement to determine if random card is the ace of diamonds
          case 1://Within switch statement, determining whether random card is an ace of diamonds
          System.out.println("the ace of diamonds.");//If case statement is true then program prints the card for user
          break; //Putting break in statement allows the switch statement to move on to the next case
          case 14: //Within switch statement, determining whether random card is an ace of clubs
          System.out.println("the ace of clubs.");//If case statement is true then program prints the card for user
          break;
          case 27://Within switch statement, determining whether random card is an ace of hearts
          System.out.println("the ace of hearts.");//If case statement is true then program prints the card for user
          break;
          case 40: //Within switch statement, determining whether random card is an ace of spades
          System.out.println("the ace of spades.");//If case statement is true then program prints the card for user
          break;}
            
        if (remainder2==1 && randomcard2<14 && randomcard2>3){ //Using if statement to determine if random card is a face card, specifically a jack of diamonds for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of diamonds");}//Printing the card if the statement is true meaning that it is the jack of diamonds
          else if (remainder2==2 && randomcard2<14 && randomcard2>3){//Using an else if statement to determine if random card is the queen of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the queen of diamonds");}//Printing the card if the statement is true meaning that it is the queen of diamonds
          else if (remainder2==3 && randomcard2<14 && randomcard2>3){//Using an else if statement to determine if random card is the king of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the king of diamonds");}//Printing the card if the statement is true meaning that it is the king of diamonds
        if (remainder2==4 && randomcard2<27 && randomcard2>16){//Using if statement to determine if random card is a face card, specifically a jack of clubs for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of clubs");}//Printing the card if the statement is true meaning that it is the jack of clubs
          else if (remainder2==5 && randomcard2<27 && randomcard2>16){//Using an else if statement to determine if random card is the queen of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the queen of clubs");}//Printing the card if the statement is true meaning that it is the queen of clubs
          else if (remainder2==6 && randomcard2<27 && randomcard2>16){//Using an else if statement to determine if random card is the king of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the king of clubs");}//Printing the card if the statement is true meaning that it is the king of clubs
        if (remainder2==7 && randomcard2<40 && randomcard2>29){//Using if statement to determine if random card is a face card, specifically a jack of hearts for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of hearts");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder2==8 && randomcard2<40 && randomcard2>29){//Using an else if statement to determine if random card is the queen of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the queen of hearts");}//Printing the card if the statement is true meaning that it is the queen of hearts
          else if (remainder2==9 && randomcard2<40 && randomcard2>29){//Using an else if statement to determine if random card is the king of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the king of hearts");}//Printing the card if the statement is true meaning that it is the king of hearts
        if (remainder2==0 && randomcard2>44){//Using if statement to determine if random card is a face card, specifically a jack of spades for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of spades");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder2==1 && randomcard2>44){//Using an else if statement to determine if random card is the queen of spades by using the remainder and specific range of numbers for spades
          System.out.println("the queen of spades");}//Printing the card if the statement is true meaning that it is the queen of spades
          else if (remainder2==2 && randomcard2>44){//Using an else if statement to determine if random card is the king of spades by using the remainder and specific range of numbers for spades
          System.out.println("the king of spades");}//Printing the card if the statement is true meaning that it is the king of spades
          
        //Fourth Card Below
        
        double random3 = (Math.random()*51)+1; //Randomly selecting a number between 1 and 52, including 1 and 52
        double randomcard3 = (int)random3; //Casting the random selection as an integer
        double remainder3 = randomcard3%10;//Using Modular function in order to get remainder
          
        if (2<=randomcard3 & randomcard3<=10){ //Using if statement to determine if random card is a numbered diamonds
        System.out.println("the "+(int)randomcard3+" of diamonds.");}//If it is a numbered diamonds then the program will print the card for user
        
        if (15<=randomcard3 & randomcard3<=23){//Using if statement to determine if random card is a numbered clubs
        double randomclubs3 = randomcard3-13; //Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomclubs3+" of clubs.");}//If it is a numbered clubs then the program will print the card for user
        
        if (28<=randomcard3 & randomcard3<=36){//Using if statement to determine if random card is a numbered hearts
        double randomhearts3 = randomcard3-26;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomhearts3+" of hearts.");}//If it is a numbered hearts then the program will print the card for user
        
        if (41<=randomcard3 & randomcard3<=49){//Using if statement to determine if random card is a numbered spades
        double randomspades3 = randomcard3-39;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomspades3+" of spades.");}//If it is a numbered spades then the program will print the card for user
        
        switch ((int)randomcard3){//Using switch statement to determine if random card is the ace of diamonds
          case 1://Within switch statement, determining whether random card is an ace of diamonds
          System.out.println("the ace of diamonds.");//If case statement is true then program prints the card for user
          break; //Putting break in statement allows the switch statement to move on to the next case
          case 14: //Within switch statement, determining whether random card is an ace of clubs
          System.out.println("the ace of clubs.");//If case statement is true then program prints the card for user
          break;
          case 27://Within switch statement, determining whether random card is an ace of hearts
          System.out.println("the ace of hearts.");//If case statement is true then program prints the card for user
          break;
          case 40: //Within switch statement, determining whether random card is an ace of spades
          System.out.println("the ace of spades.");//If case statement is true then program prints the card for user
          break;}
            
        if (remainder3==1 && randomcard3<14 && randomcard3>3){ //Using if statement to determine if random card is a face card, specifically a jack of diamonds for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of diamonds");}//Printing the card if the statement is true meaning that it is the jack of diamonds
          else if (remainder3==2 && randomcard3<14 && randomcard3>3){//Using an else if statement to determine if random card is the queen of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the queen of diamonds");}//Printing the card if the statement is true meaning that it is the queen of diamonds
          else if (remainder3==3 && randomcard3<14 && randomcard3>3){//Using an else if statement to determine if random card is the king of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the king of diamonds");}//Printing the card if the statement is true meaning that it is the king of diamonds
        if (remainder3==4 && randomcard3<27 && randomcard3>16){//Using if statement to determine if random card is a face card, specifically a jack of clubs for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of clubs");}//Printing the card if the statement is true meaning that it is the jack of clubs
          else if (remainder3==5 && randomcard3<27 && randomcard3>16){//Using an else if statement to determine if random card is the queen of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the queen of clubs");}//Printing the card if the statement is true meaning that it is the queen of clubs
          else if (remainder3==6 && randomcard3<27 && randomcard3>16){//Using an else if statement to determine if random card is the king of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the king of clubs");}//Printing the card if the statement is true meaning that it is the king of clubs
        if (remainder3==7 && randomcard3<40 && randomcard3>29){//Using if statement to determine if random card is a face card, specifically a jack of hearts for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of hearts");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder3==8 && randomcard3<40 && randomcard3>29){//Using an else if statement to determine if random card is the queen of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the queen of hearts");}//Printing the card if the statement is true meaning that it is the queen of hearts
          else if (remainder3==9 && randomcard3<40 && randomcard3>29){//Using an else if statement to determine if random card is the king of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the king of hearts");}//Printing the card if the statement is true meaning that it is the king of hearts
        if (remainder3==0 && randomcard3>44){//Using if statement to determine if random card is a face card, specifically a jack of spades for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of spades");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder3==1 && randomcard3>44){//Using an else if statement to determine if random card is the queen of spades by using the remainder and specific range of numbers for spades
          System.out.println("the queen of spades");}//Printing the card if the statement is true meaning that it is the queen of spades
          else if (remainder3==2 && randomcard3>44){//Using an else if statement to determine if random card is the king of spades by using the remainder and specific range of numbers for spades
          System.out.println("the king of spades");}//Printing the card if the statement is true meaning that it is the king of spades
        
        //Fifth card below
        
        double random4 = (Math.random()*51)+1; //Randomly selecting a number between 1 and 52, including 1 and 52
        double randomcard4 = (int)random4; //Casting the random selection as an integer
        double remainder4 = randomcard4%10;//Using Modular function in order to get remainder
          
        if (2<=randomcard4 & randomcard4<=10){ //Using if statement to determine if random card is a numbered diamonds
        System.out.println("the "+(int)randomcard4+" of diamonds.");}//If it is a numbered diamonds then the program will print the card for user
        
        if (15<=randomcard4 & randomcard4<=23){//Using if statement to determine if random card is a numbered clubs
        double randomclubs4 = randomcard4-13; //Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomclubs4+" of clubs.");}//If it is a numbered clubs then the program will print the card for user
        
        if (28<=randomcard4 & randomcard4<=36){//Using if statement to determine if random card is a numbered hearts
        double randomhearts4 = randomcard4-26;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomhearts4+" of hearts.");}//If it is a numbered hearts then the program will print the card for user
        
        if (41<=randomcard4 & randomcard4<=49){//Using if statement to determine if random card is a numbered spades
        double randomspades4 = randomcard4-39;//Using subtraction to determine the number on the card if it is a numbered card
        System.out.println("the "+(int)randomspades4+" of spades.");}//If it is a numbered spades then the program will print the card for user
        
        switch ((int)randomcard4){//Using switch statement to determine if random card is the ace of diamonds
          case 1://Within switch statement, determining whether random card is an ace of diamonds
          System.out.println("the ace of diamonds.");//If case statement is true then program prints the card for user
          break; //Putting break in statement allows the switch statement to move on to the next case
          case 14: //Within switch statement, determining whether random card is an ace of clubs
          System.out.println("the ace of clubs.");//If case statement is true then program prints the card for user
          break;
          case 27://Within switch statement, determining whether random card is an ace of hearts
          System.out.println("the ace of hearts.");//If case statement is true then program prints the card for user
          break;
          case 40: //Within switch statement, determining whether random card is an ace of spades
          System.out.println("the ace of spades.");//If case statement is true then program prints the card for user
          break;}
            
        if (remainder4==1 && randomcard4<14 && randomcard4>3){ //Using if statement to determine if random card is a face card, specifically a jack of diamonds for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of diamonds");}//Printing the card if the statement is true meaning that it is the jack of diamonds
          else if (remainder4==2 && randomcard4<14 && randomcard4>3){//Using an else if statement to determine if random card is the queen of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the queen of diamonds");}//Printing the card if the statement is true meaning that it is the queen of diamonds
          else if (remainder4==3 && randomcard4<14 && randomcard4>3){//Using an else if statement to determine if random card is the king of diamonds by using the remainder and specific range of numbers for diamonds
          System.out.println("the king of diamonds");}//Printing the card if the statement is true meaning that it is the king of diamonds
        if (remainder4==4 && randomcard4<27 && randomcard4>16){//Using if statement to determine if random card is a face card, specifically a jack of clubs for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of clubs");}//Printing the card if the statement is true meaning that it is the jack of clubs
          else if (remainder4==5 && randomcard4<27 && randomcard4>16){//Using an else if statement to determine if random card is the queen of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the queen of clubs");}//Printing the card if the statement is true meaning that it is the queen of clubs
          else if (remainder4==6 && randomcard4<27 && randomcard4>16){//Using an else if statement to determine if random card is the king of clubs by using the remainder and specific range of numbers for clubs
          System.out.println("the king of clubs");}//Printing the card if the statement is true meaning that it is the king of clubs
        if (remainder4==7 && randomcard4<40 && randomcard4>29){//Using if statement to determine if random card is a face card, specifically a jack of hearts for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of hearts");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder4==8 && randomcard4<40 && randomcard4>29){//Using an else if statement to determine if random card is the queen of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the queen of hearts");}//Printing the card if the statement is true meaning that it is the queen of hearts
          else if (remainder4==9 && randomcard4<40 && randomcard4>29){//Using an else if statement to determine if random card is the king of hearts by using the remainder and specific range of numbers for hearts
          System.out.println("the king of hearts");}//Printing the card if the statement is true meaning that it is the king of hearts
        if (remainder4==0 && randomcard4>44){//Using if statement to determine if random card is a face card, specifically a jack of spades for this statement. Using the remainder to help determine this for specific range of numbers
          System.out.println("the jack of spades");}//Printing the card if the statement is true meaning that it is the jack of hearts
          else if (remainder4==1 && randomcard4>44){//Using an else if statement to determine if random card is the queen of spades by using the remainder and specific range of numbers for spades
          System.out.println("the queen of spades");}//Printing the card if the statement is true meaning that it is the queen of spades
          else if (remainder4==2 && randomcard4>44){//Using an else if statement to determine if random card is the king of spades by using the remainder and specific range of numbers for spades
          System.out.println("the king of spades");}//Printing the card if the statement is true meaning that it is the king of spades
        
        if (randomcard==randomspades||randomcard==randomspades1||randomcard==randomspades2||randomcard==randomspades3||randomcard==randomspades4){
          System.out.println("you have a pair");}
        else if (randomcard==randomhearts||randomcard==randomhearts1||randomcard==randomhearts2||randomcard==randomhearts3||randomcard==randomhearts4){
          System.out.println("you have a pair");}
        else if (randomcard==randomclubs||randomcard==randomclubs1||randomcard==randomclubs2||randomcard==randomclubs3||randomcard==randomclubs4){
          System.out.println("you have a pair");}
        else if (randomspades==randomhearts||randomspades==randomhearts1||randomspades==randomhearts2||randomspades==randomhearts3||randomspades==randomhearts4){
          System.out.println("you have a pair");}
        else if (randomspades==randomcard1||randomspades==randomcard2||randomspades==randomcard3||randomspades==randomcard4){
          System.out.println("you have a pair");}
        
        
        
        
                    
}  //end of main method   
  	} //end of class