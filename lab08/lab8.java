////////Program
// CSE 2,Lab08
import java.util.Arrays;
import java.util.Random;
public class lab8{
    public static void main(String[] args) {
      int num = (int)(Math.random()*(50)) + 50;
      int[] myIntArray = new int[num];
      System.out.println("The size of the array is " + num);
      Random myrandom = new Random();
      for (int i = 0; i<num; i++){
         myIntArray[i] = myrandom.nextInt(99);
         System.out.println(myIntArray[i] + " ");}
      double mean = findmean(myIntArray, num);
      int range = findrange(myIntArray);
      double stdev = findsd(myIntArray,mean,num);
      System.out.println("The mean is: " + mean);
      System.out.println("The range is: " + range);
      System.out.println("The standard deviation is: " + stdev);
      shuffle(myIntArray,num);
    }

    public static double findmean(int[] myIntArray, int num) {
      double sum = 0;
      for (int i = 0; i<num; i++){
        sum = sum + myIntArray[i];}
      double average = sum/num;
      return average;
      }
  
    
    public static int findrange(int[] myIntArray) {
      Arrays.sort(myIntArray);
      int range = (myIntArray[myIntArray.length-1] - myIntArray[0]);
      return range;
    }
    
    public static double findsd(int[] myIntArray, double mean, int num) {
      double stand = 0;
	  for(int i = 0; i < num; i++) {
      stand += Math.pow((myIntArray[i] - mean), 2);}
      stand /= (num - 1);
      stand = Math.sqrt(stand);
      return stand;
      
    }
    
    public static void shuffle(int[] myIntArray, int num){
      for(int i = 0; i < num; i++){ 
        int shuff = (int)(num * Math.random()); 
        int sort = myIntArray[shuff]; 
        myIntArray[shuff] = myIntArray[i]; 
        myIntArray[i] = sort; 
        System.out.println(myIntArray[i]);}
    }


}